var dssv = [];
// lấy thông tin từ localstorage lên lại
var dataJson = localStorage.getItem("DSSV");
if (dataJson !=null) {
dssv = JSON.parse(dataJson).map(function(item) {
  console.log('item:', item);
  return new sinhVien(
    item.ma,
    item.ten,
    item.email,
    item.matKhau,
    item.toan,
    item.ly,
    item.hoa
    );
});
renderDSSV(dssv);
}

// hàm chính
function themSinhVien() {
  // lấy thông tin từ giao diện
  var sv = layThongTinTuForm();
  dssv.push(sv);
  //   render dssv
  renderDSSV(dssv);
  // Lưu dssv localstorage
  // localStorage: Nơi lưu trữ (Chỉ chấp nhận json); json: 1 loại dữ liệu
  var dataJson = JSON.stringify(dssv);
  localStorage.setItem("DSSV", dataJson);
}
// xóa sinh viên
function xoaSinhVien(id) {
  console.log('id',id);
  // splice finIndex
  var index = dssv.findIndex(function (item) {
    console.log("item", item);
    return item.ma == id;
  });
  dssv.splice(index, 1);
  console.log("dssv", dssv);
  renderDSSV(dssv);
}

// sửa sinh viên 
function suaSinhVien(id) {
  console.log('id',id);
  // tìm vị trí sinh viên trong dssv có mã trùng với ud của onclick 
  var index = dssv .findIndex(function(item) {
    return item.ma == id;
  });
  showThongTinLenForm(dssv[index]);

}